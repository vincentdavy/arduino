//
// Created by vincent on 23/12/2020.
//

#include "InterruptCounter.h"

void InterruptCounter::incrementCounter() {
    counter++;
}

HardwareSerial *operator<<(HardwareSerial *hwSerial, const InterruptCounter &interruptCounter) {
    char *text;
    text = (char *) malloc(sizeof(char) * 5);
    sprintf(text, "%d\n", interruptCounter.counter);
    hwSerial->print(text);
    free(text);
    return hwSerial;
}
