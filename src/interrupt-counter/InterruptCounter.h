//
// Created by vincent on 23/12/2020.
//

#ifndef UNO_TEST_INTERRUPTCOUNTER_H
#define UNO_TEST_INTERRUPTCOUNTER_H


#include <HardwareSerial.h>

class InterruptCounter {
private:
    unsigned int counter = 0;
public:
    void incrementCounter();

    friend HardwareSerial *operator<<(HardwareSerial *hwSerial, InterruptCounter const &interruptCounter);
};


#endif //UNO_TEST_INTERRUPTCOUNTER_H
