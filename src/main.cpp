#include <Arduino.h>
#include "airboard/airboard.h"

#ifdef DEBUG
#include "avr8-stub.h"
#else

#include "my-serial/my-serial.h"
#include <interrupt-counter/InterruptCounter.h>

HardwareSerial *mySerial = nullptr;
InterruptCounter interruptCounter;
#endif

volatile bool interruptIntermediate = false;
volatile bool interruptFlag = false;
const int interruptPin = 2;
const int leds[] = {GREEN, RED, BLUE};

//ISR(TIMER0_COMPA_vect) {
////    interruptIntermediate = true;
//    interruptFlag = true;
//}
//
//
//ISR(TIMER0_COMPB_vect) {
//    if (interruptIntermediate) {
//        interruptIntermediate = false;
//        interruptFlag = true;
//    }
//}

void setup() {
#ifdef DEBUG
    debug_init();
    breakpoint();
    debug_message("Debugger waiting");
#else
    mySerial = serialInit();
    *mySerial << "Boot\n";

//    pinMode(interruptPin, INPUT_PULLUP);
//    attachInterrupt(digitalPinToInterrupt(interruptPin), interrupt, RISING);
#endif

    pinMode(GREEN, OUTPUT);
    pinMode(RED, OUTPUT);
    pinMode(BLUE, OUTPUT);
    pinMode(VDD, OUTPUT);
    digitalWrite(VDD, LOW);

    TCCR1A = bit(COM1A1) | /*_BV(COM0A0) | _BV(COM0B1) | */ bit(WGM11) | _BV(WGM10);
    TCCR1B = bit(WGM12) | bit(CS12) | _BV(CS10);
    OCR1A = 0x8;
//    OCR0B = 0x10;
//    TIMSK1 = /*1 << OCIE0B |*/ 1 << OCIE1A;
}

void loop() {
#ifndef DEBUG
    if (interruptFlag) {
        interruptCounter.incrementCounter();
        mySerial << String("interrupt ! ") << interruptCounter;
        interruptFlag = false;
    }
#endif
//    for (int led : leds) {
//        analogWrite(led, 0x7f);
//        delay(150);
//        analogWrite(led, 0xff);
//        delay(150);
//        analogWrite(led, 0x0);
//    }
}

