//
// Created by vincent on 23/12/2020.
//
//
#ifndef DEBUG

#include "Arduino.h"
#include "my-serial.h"

HardwareSerial *serialInit() {
    Serial.begin(115200);
    while (!Serial);
    return &Serial;
}

HardwareSerial *operator<<(HardwareSerial *hwSerial, String const &text) {
    hwSerial->print(text);
    return hwSerial;
}

HardwareSerial &operator<<(HardwareSerial &hwSerial, const char * text) {
    hwSerial.print(text);
    return hwSerial;
}

void interrupt() {
    interruptFlag = true;
}

#endif
