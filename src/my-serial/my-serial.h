//
// Created by vincent on 23/12/2020.
//

#ifndef UNO_TEST_MY_SERIAL_H
#define UNO_TEST_MY_SERIAL_H

extern volatile bool interruptFlag;

HardwareSerial *serialInit();

HardwareSerial *operator<<(HardwareSerial *hwSerial, String const &text);

HardwareSerial &operator<<(HardwareSerial &hwSerial, const char * text);

void interrupt();

#endif //UNO_TEST_MY_SERIAL_H
