//
// Created by vincent on 24/12/2020.
//

#ifndef UNO_TEST_AIRBOARD_H
#define UNO_TEST_AIRBOARD_H

#define GREEN     5 // GREEN LED (PWM)
#define BLUE     6 // BLUE LED (PWM)
#define RED         9 // RED LED (PWM)
#define VUSB    A6 // USB voltage indicator
#define VBAT    A7 // battery voltage indicator
#define VDD     A0 // Switchable output


#endif //UNO_TEST_AIRBOARD_H
